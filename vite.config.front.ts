import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import Unocss from 'unocss/vite'

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    'APP_VERSION': JSON.stringify(process.env.npm_package_version),
  },
  root: resolve(__dirname, './src/front/'),
  build: {
    outDir: resolve(__dirname, './build/front')
  },
  plugins: [vue(), Unocss()]
}
)
