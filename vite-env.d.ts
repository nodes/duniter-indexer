import { ApiPromise } from '@polkadot/api'

declare global {
  const APP_VERSION: string

  interface Window {
    INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL: string
    INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL: string
    polkadot: ApiPromise
  }
}