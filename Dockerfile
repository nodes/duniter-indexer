FROM docker.io/library/node:18-alpine
ENV NODE_ENV=PRODUCTION

LABEL maintainer="Emmanuel Salomon @ https://axiom-team.fr"

# Copy source code
COPY --chown=node:node package*.json ./

# Running npm install
RUN npm install && npm cache clean --force

# Copy the rest of your app's source code from your host to your image filesystem.
COPY --chown=node:node . .

RUN npm run build && npm run build:front

# Mount points
RUN mkdir /logs
RUN chown node:node /logs
VOLUME /logs /resources

# Switch to 'node' user
USER node

# Open the mapped port
EXPOSE 3000

CMD ["node", "build/server.mjs"]
