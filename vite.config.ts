import { defineConfig } from 'vite'
import { resolve } from 'path'
import { configDefaults } from 'vitest/config'


export default defineConfig({
  server: {
    // vite server configs, for details see [vite doc](https://vitejs.dev/config/#server-host)
    port: 3000
  },
  plugins: [],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src')
    }
  },
  test: {
    exclude: [...configDefaults.exclude, 'postgres-data']
  }
})
