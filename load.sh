#!/bin/bash

echo -e "Start indexer in dev mode\n"

## Remove all volumes
docker compose -f docker-duniter-dev.yml -f docker-dep-dev.yml down -v

# Start the dev environment
docker compose -f docker-duniter-dev.yml up -d # start duniter node in dev mode with custom genesis
# ➡️ https://polkadot.js.org/apps/?rpc=ws://localhost:9944#/explorer
docker compose -f docker-dep-dev.yml up -d # start postgres database and hasura grpahql engine (can take about 30 seconds to initialize)
# ➡️ http://localhost:8080/console

# Start the indexer
pnpm dev:test # see in package.json what it's doing

# Alternatively press F5 for vscode debugging
# see in .vscode/launch.json what it does
