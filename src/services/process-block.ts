/*
From Polkadot API ugly example:
https://polkadot.js.org/docs/api/cookbook/blocks/
*/
import { AbstractArray } from '@polkadot/types-codec/abstract/Array'
import type { IEventRecord } from '@polkadot/types/types/events'

import type { SignedBlock } from '@polkadot/types/interfaces/runtime'
import { Codec } from '@polkadot/types/types'
import { FastifyInstance } from 'fastify'

export default async function processBlock(
  app: FastifyInstance,
  signedBlock: SignedBlock
): Promise<Boolean> {
  let isBlockIndexed = false

  // Process only if there are more than 1 extrinsic. Each block contains a 'timestamp.set' section.method
  if (signedBlock.block.extrinsics.length > 1) {
    const blockNumber = signedBlock.block.header.number.toNumber()
    const blockHash = signedBlock.block.hash.toString()

    const apiAt = await app.polkadot.at(blockHash)
    const allRecords = await apiAt.query.system.events<
      AbstractArray<IEventRecord<Codec[]>>
    >()

    // Map between the extrinsics and events
    // Extract information for each events
    let events = []
    for (const [
      number,
      {
        isSigned,
        method: { args, method, section },
        signer,
        nonce
      }
    ] of signedBlock.block.extrinsics.entries()) {
      // Filter events only where extrinsic was applied
      const appliedEvents = allRecords.filter(
        ({ phase }) =>
          phase.isApplyExtrinsic && phase.asApplyExtrinsic.eq(number)
      )

      // Check in all events if the extrinsic is successed and contains more events than only timestamp event
      const isSuccess = appliedEvents.some(
        ({ event }) =>
          event &&
          app.polkadot.events.system.ExtrinsicSuccess.is(event) &&
          section !== 'timestamp'
      )

      // If the extrinsic is valid, process it.
      if (isSuccess) {
        for (const { event } of appliedEvents) {
          // Check if there are events different than extrinsic success
          if (event.method.toString() !== 'ExtrinsicSuccess')
            events.push({
              eventName: `${event.section.toString()}.${event.method.toString()}`,
              event,
              args,
              signer: signer.toString()
            })
        }
      }
    }

    // Filter and insert events
    if (events.length) {
      // Prioritize events
      const order = ['system.NewAccount', 'identity.IdtyCreated']
      events = events.sort(function (a, b) {
        if (!order.includes(a.eventName)) return 1
        if (!order.includes(b.eventName)) return -1
        return order.indexOf(a.eventName) - order.indexOf(b.eventName)
      })

      try {
        // Extract block date. Ugly, I can't find a better way to get the date of the block ?
        // The only polkadot issue for that : https://github.com/polkadot-js/api/issues/2603
        let now = signedBlock.block.extrinsics.filter(
          ({ method: { section, method } }) =>
            section === 'timestamp' && method === 'set'
        )[0].args[0]

        const created_at = new Date(Number(now))

        // Save this block because something cool happened here!
        await app.sql`
        INSERT INTO block ${app.sql({
          number: blockNumber,
          hash: blockHash,
          created_at,
          data: {
            extrinsics: signedBlock.block.extrinsics.toHuman(),
            events: allRecords.toHuman()
          }
        })}
        `
        isBlockIndexed = true

        app.log.info(`Processing block ${blockNumber}`)

        // console.log(events)

        for (const { eventName, event, args, signer } of events) {
          // Emit event. Listeners are registred in folder /processors
          app.blockEvent.emit(eventName, {
            blockIndex: blockNumber,
            blockHash,
            created_at,
            event,
            args,
            signer: signer.toString()
          })

          // Save unhandled event name in db
          if (app.blockEvent.listenerCount(eventName) === 0) {
            app.log.warn(`[${blockNumber}] Unhandled event: ${eventName}`)
            await app.sql`
              UPDATE parameters SET value = value || ${eventName}
              WHERE key = 'non_indexed_events' AND NOT (value @> ${eventName}) 
            `
          }
        }
      } catch (error) {
        app.log.error(error)
      }
    }
  }

  return isBlockIndexed
}
