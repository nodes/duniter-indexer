import { performance } from 'perf_hooks'
import indexGenesis from '@/services/index-genesis'
import indexPastTx from '@/services/index-past-tx'
import processBlock from '@/services/process-block'
import type { FastifyInstance } from 'fastify'

export interface GenesisInfo {
  block0Hash: string
  block1Date: Date
}

export default async function indexBlocks(app: FastifyInstance) {
  const t0 = performance.now()
  
  try {
    // Get the latest indexed block
    const lastIndex = (
      await app.sql`
      SELECT value as last_indexed_block_number
      FROM parameters
      WHERE key = 'last_indexed_block_number'
    `
    )[0]
    const last_indexed_block_number = lastIndex?.last_indexed_block_number || 0

    // Index genesis
    // if it is not already done
    if (last_indexed_block_number === 0) {
      // get block 0 hash and block 1 time from rpc api
      const block0Hash = (await app.polkadot.rpc.chain.getBlockHash(0)).toString()
      const block1Hash = (await app.polkadot.rpc.chain.getBlockHash(1)).toString()
      const block1 = await app.polkadot.rpc.chain.getBlock(block1Hash)
      const created_at = new Date(
        +block1.block.extrinsics[0].method.args[0].toString()
      )
      const genesis_info: GenesisInfo = {
        "block0Hash": block0Hash,
        "block1Date": created_at,
      }
      await indexGenesis(app, genesis_info)
      await indexPastTx(app, genesis_info)
      app.log.info('Indexing past blocks...')
    }

    // Get the latest indexed block hash
    const lastHash = (
      await app.sql`
      SELECT value as last_indexed_block_hash
      FROM parameters
      WHERE key = 'last_indexed_block_hash'
    `
    )[0]
    const last_indexed_block_hash = lastHash?.last_indexed_block_hash || null
    // Index past blocks
    let indexed_blocks_count = 0 // We don't count the genesis block
    let number = last_indexed_block_number
    let finalized_block_hash = (
      await app.polkadot.rpc.chain.getFinalizedHead()
    ).toString()

    if (last_indexed_block_hash !== finalized_block_hash) {
      app.log.info('There are some blocks to index')
      let blockHash

      do {
        number++
        if (number % 100 === 0) {
          app.log.info(`Indexing block #${number}`)
        }
        // Get the hash of the block n+1
        blockHash = (
          await app.polkadot.rpc.chain.getBlockHash(number)
        ).toString()

        // If block number greater than current blockchain finalized block number, stop iterate.
        if (
          blockHash ===
          '0x0000000000000000000000000000000000000000000000000000000000000000'
        )
          break

        // Process block
        const block = await app.polkadot.rpc.chain.getBlock(blockHash)
        try {
          if (await processBlock(app, block)) indexed_blocks_count++
        } catch (error) {
          app.log.warn(
            {
              number,
              blockHash,
              error,
              block
            },
            'Indexing past blocks failed'
          )
        }

        // Write the block number and hash to the database
        await app.sql`UPDATE parameters SET value = ${number} WHERE key = 'last_indexed_block_number'`
        await app.sql`UPDATE parameters SET value = ${blockHash} WHERE key = 'last_indexed_block_hash'`

        // Get the latest block in substrate
        finalized_block_hash = (
          await app.polkadot.rpc.chain.getFinalizedHead()
        ).toString()
        // } while (number < 9100) // limit indexing for testing
      } while (blockHash !== finalized_block_hash)
    }

    // Index past blocks done. Log info.
    const duration = performance.now() - t0
    if (indexed_blocks_count > 1)
      app.log.info(
        {
          duration: duration.toFixed(2),
          duration_per_block: duration / (number - last_indexed_block_number),
          processed_blocks_count: number - last_indexed_block_number,
          indexed_blocks_count,
          last_indexed_block_number,
          last_indexed_block_hash,
          finalized_block_hash
          // genesis: api.genesisHash.toHex(),
          // chain: await api.rpc.chain.getHeader(),
          // chainInfo: await api.registry.getChainProperties(),
          // test: await api.rpc.state.getPairs('0x')
          // query block finalisé qui ne change jamais.
          // api.rpc.chain.subscribeFinalizedHeads
          // events: await api.rpc.state.getStorage('0x26aa394eea5630e07c48ae0c9558cef780d41e5e16056765bc8461851072c9d7'),
          // lastFinalizedBlock: await api.rpc.chain.getFinalizedHead()
          // get block
          // api.rpc.chain.getFinalizedHead
        },
        'Indexing past blocks done'
      )
  } catch (error: any) {
    app.log.error(error.message, 'Indexing past blocks failed')
  }
}
