import indexBlocks from '@/services/index-blocks'
import type { FastifyInstance } from 'fastify'

let indexingLock = false

export default function subscribeNewBlock(app: FastifyInstance) {
  app.polkadot.rpc.chain.subscribeFinalizedHeads(async (header) => {
    app.log.info(`Last finalized from RPC: ${+header.number}`)
    // Get the hash of the block
    if (indexingLock) {
      app.log.warn(`Already indexing.`)
      return
    }
    const blockHash = (
      await app.polkadot.rpc.chain.getBlockHash(+header.number)
    ).toString()

    // Process block
    try {
      indexingLock = true
      await indexBlocks(app)
    } catch (e) {
      app.log.warn(`${header} ${blockHash}`)
    } finally {
      indexingLock = false
    }

    try {
      // Write the block number and hash to the database
      await app.sql`UPDATE parameters SET value = ${+header.number} WHERE key = 'last_indexed_block_number'`
      await app.sql`UPDATE parameters SET value = ${blockHash} WHERE key = 'last_indexed_block_hash'`
    } catch (err: any) {
      app.log.error(err.message)
    }
  })

  /*
  await api.rpc.state.subscribeStorage(['0x26aa394eea5630e07c48ae0c9558cef7b99d880ec681799c0cf30e8886371da9'], (data) => {
    console.log(data)
})
*/
}
