import indexBlocks from '@/services/index-blocks'
import subscribeNewBlock from '@/services/subscribe-new-block'
import type { FastifyInstance } from 'fastify'

export default function (app: FastifyInstance, options: unknown, done: () => void) {
  // Wait for server ready
  app.ready(() => {
    // Use timeout to start indexing after fastify ready
    setTimeout(async () => {
      // Index past blocks
      await indexBlocks(app)

      // Subscribe to new blocks
      subscribeNewBlock(app)
    }, import.meta.env.PROD ? 30000 : 10) // wait for hasura metadata applied
  })

  done()
}
