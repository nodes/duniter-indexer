import { readFileSync, writeFileSync } from 'fs'
import { resolve } from 'path'
import { cwd } from 'process'
import config from '@/config'
import type { FastifyInstance } from 'fastify'
import type { GenesisInfo } from './index-blocks'
import type { Genesis } from './index-genesis'

type address = string
type TransactionHistory = Map<address, Array<Tx>>
interface Tx {
  issuer: address
  amount: string
  written_time: number
  comment: string
}

export default async function indexPastTx(
  app: FastifyInstance,
  genesis_info: GenesisInfo
) {
  // Read transaction history json
  const filePath = resolve(
    cwd(),
    config.INDEXER_TRANSACTIONS_FILE_PATH as string
  )
  const resp = readFileSync(filePath)
  const data: JSON = JSON.parse(resp.toString())
  app.log.debug(`Transaction history data length ${Object.keys(data).length}`)
  const tx_history: TransactionHistory = new Map<address, Array<Tx>>(
    Object.entries(data)
  )
  // app.log.debug(`Transaction history tx_history size ${tx_history.size}`)
  const created_at = genesis_info.block1Date
  const created_at_str = created_at.toJSON().replace(/Z$/, '+00:00') // format creation date

  // also read genesis to get list of accounts
  const genesis: Genesis = JSON.parse(
    readFileSync(
      resolve(cwd(), config.INDEXER_GENESIS_FILE_PATH as string)
    ).toString()
  ).genesis.runtime
  const genesis_wallets = new Set<address>()
  for (const [address, _] of Object.entries(genesis.account.accounts)) {
    genesis_wallets.add(address)
  }

  app.log.info(`Indexing past transactions`)

  // set of wallets to make sure they are created in database
  const all_wallets = new Set<address>()

  // add all issuers and receivers
  for (const [receiver, txs] of tx_history.entries()) {
    all_wallets.add(receiver)
    for (const tx of txs) {
      all_wallets.add(tx.issuer)
      // app.log.info(`. ${all_wallets.size}`)
    }
  }

  // apparently Set difference is experimental
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/difference
  // const unknown_wallets = wallets.difference(genesis_wallets)
  let unknown_wallets = new Set(
    [...all_wallets].filter((x) => !genesis_wallets.has(x))
  )

  app.log.info(
    `There are ${genesis_wallets.size} genesis wallets, ${all_wallets.size} total wallets, ${unknown_wallets.size} unknown wallets`
  )

  if (unknown_wallets.size != 0) {
    // create wallets that do not exist yet in database
    // database only contains wallets with non-empty amount
    // but transaction history can include account that were emptied later
    app.log.info(
      `Adding ${unknown_wallets.size} wallets not included in genesis`
    )

    // Insert these wallets (batch)
    var query = 'INSERT INTO account (pubkey, created_at, created_on) VALUES\n' // base of query
    for (const address of unknown_wallets) {
      query += `('${address}', '${created_at_str}', 0),\n`
    }
    query = query.replace(/,\n$/, ';') // end of query
    var tmp = resolve(cwd(), './tmp/tx_unknown_wallets.sql') // place to temporarily store query
    writeFileSync(tmp, query)
    await app.sql.file(tmp) // submit query
  }

  // once we are sure all wallets do exist, we can add txs
  app.log.info(`Adding transaction history in a batch`)
  var query =
    'INSERT INTO transaction (receiver_pubkey, issuer_pubkey, amount, comment, created_at, created_on) VALUES\n' // base of query
  for (const [receiver, txs] of tx_history.entries()) {
    for (const tx of txs) {
      // FIXME skip transaction with null written_time
      if (tx.written_time != null) {
        const date = new Date(tx.written_time * 1000) // seconds to milliseconds
        const date_str = date.toJSON().replace(/Z$/, '+00:00') // sql format of date
        query += `('${receiver}', '${tx.issuer}', '${tx.amount}', '${tx.comment}', '${date_str}', 0),\n`
      }
    }
  }
  query = query.replace(/,\n$/, ';') // end of query
  var tmp = resolve(cwd(), './tmp/tx_history.sql') // place to temporarily store query
  writeFileSync(tmp, query)
  await app.sql.file(tmp) // submit query
}
