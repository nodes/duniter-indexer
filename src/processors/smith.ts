import type { FastifyInstance } from 'fastify'

export default async (app: FastifyInstance) => {
  async function renewSmithCert({
    blockIndex,
    created_at,
    args,
    signer
  }: newCertArgs) {
    const data = {
      issuer_index: Number(args[0]),
      receiver_index: Number(args[1]),
      created_at: created_at,
      created_on: blockIndex
    }
    // ============== begin of dirty fix ==============
    // checks that the receiver of the smith cert exists in the smith table
    // FIXME only useful because of Duniter bug making that smith certifications can be emitted to an identity who did requested smith membership
    var existing =
      await app.sql`SELECT idty_index FROM smith WHERE idty_index = ${data.receiver_index}`
    var existing_int: Array<number> = existing.map((e) => e.idty_index)
    if (!existing_int.includes(data.receiver_index)) {
      app.log.info('add smith number ' + data.receiver_index)
      const smith = { idty_index: data.receiver_index }
      await app.sql`INSERT INTO smith ${app.sql(smith)}`
    }
    // also do this for issuer since smith membership requests are not handled
    var existing =
      await app.sql`SELECT idty_index FROM smith WHERE idty_index = ${data.issuer_index}`
    var existing_int: Array<number> = existing.map((e) => e.idty_index)
    if (!existing_int.includes(data.issuer_index)) {
      app.log.info('add smith number ' + data.issuer_index)
      const smith = { idty_index: data.issuer_index }
      await app.sql`INSERT INTO smith ${app.sql(smith)}`
    }
    // ============== end of dirty fix ==============


    // insert the smith certification
    await app.sql`INSERT INTO smith_cert ${app.sql(data)}`
    app.log.warn(
      `[${blockIndex}] Insert smith certification ${data.issuer_index} -> ${data.receiver_index}`
    )
  }

  // add events handlers
  app.blockEvent.on('smithCert.NewCert', renewSmithCert)
  app.blockEvent.on('smithCert.RenewedCert', renewSmithCert)
  // also add this for previous runtime
  app.blockEvent.on('smithsCert.NewCert', renewSmithCert)
  app.blockEvent.on('smithsCert.RenewedCert', renewSmithCert)
}
