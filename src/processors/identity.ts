import type { FastifyInstance } from 'fastify'

export default async (app: FastifyInstance) => {
  // app.blockEvent.on(eventName, async ({ blockIndex, blockHash, created_at, event, args, signer }) => {
    app.blockEvent.on(
      'identity.IdtyCreated',
      async ({ blockIndex, created_at, args, event, signer }: idtyCreatedArgs) => {
      const data = {
        confirmed_at: created_at,
        confirmed_on: blockIndex,
        index: event.data[0].toNumber(),
        pubkey: event.data[1].toString()
      }
      await app.sql`INSERT INTO identity ${app.sql(data)}`
      app.log.warn(`[${blockIndex}] Create identity ${data.index}`)
    }
  )

  app.blockEvent.on(
    'identity.IdtyConfirmed',
    async ({ blockIndex, created_at, args, signer }: idtyConfirmedArgs) => {
      const data = {
        pubkey: signer,
        confirmed_at: created_at,
        confirmed_on: blockIndex,
        name: args.toString()
      }
      await app.sql`
      UPDATE identity SET ${app.sql(data)}
      WHERE pubkey = ${signer}
      `
      app.log.warn(`[${blockIndex}] Confirm identity ${data.name}`)
    }
  )

  app.blockEvent.on(
    'identity.IdtyValidated',
    async ({ blockIndex, created_at, signer }: idtyValidatedArgs) => {
      const data = {
        pubkey: signer,
        validated_at: created_at,
        validated_on: blockIndex
      }
      await app.sql`
      UPDATE identity SET ${app.sql(data)}
      WHERE pubkey = ${data.pubkey}
    `
      app.log.warn(`[${blockIndex}] Validate identity ${data.pubkey}`)
    }
  )

  app.blockEvent.on(
    'identity.IdtyChangedOwnerKey',
    async ({ blockIndex, event, signer }: idtyChangedOwnerKeyArgs) => {
      const data = {
        oldPubkey: signer,
        pubkey: event.data.newOwnerKey.toString()
      }
      await app.sql`
      UPDATE identity SET ${app.sql(data, 'pubkey')}
      WHERE pubkey = ${data.oldPubkey}
    `
      app.log.warn(
        `[${blockIndex}] Migrate identity ${data.oldPubkey} -> ${data.pubkey}`
      )
    }
  )

  app.blockEvent.on(
    'identity.IdtyRemoved',
    async ({ blockIndex, created_at, signer }: idtyRemovedArgs) => {
      const data = {
        pubkey: signer,
        revoked_at: created_at,
        revoked_on: blockIndex
      }
      await app.sql`
        UPDATE identity SET ${app.sql(data)}
        WHERE pubkey = ${data.pubkey}
      `
      app.log.warn(`[${blockIndex}] Remove identity ${data.pubkey}`)
    }
  )
}
