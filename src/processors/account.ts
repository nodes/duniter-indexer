import type { FastifyInstance } from 'fastify'

export default async (app: FastifyInstance) => {
  // app.blockEvent.on(eventName, async ({ blockIndex, blockHash, created_at, event, args, signer }) => {

  async function insertAccount({
    blockIndex,
    created_at,
    event
  }: insertAccountArgs) {
    const data = {
      pubkey: event.data[0].toString(),
      created_at: created_at,
      created_on: blockIndex
    }
    await app.sql`INSERT INTO account ${app.sql(data)} ON CONFLICT DO NOTHING`
    app.log.warn(`[${blockIndex}] Insert account ${data.pubkey}`)
  }
  app.blockEvent.on('balances.Endowed', insertAccount)
  app.blockEvent.on('system.NewAccount', insertAccount)
}
