import type { FastifyInstance } from 'fastify'

export default async (app: FastifyInstance) => {
  app.blockEvent.on(
    'balances.Transfer',
    async ({ blockIndex, created_at, event }: insertAccountArgs) => {
      try {
        // TODO: check and create pubkey if it doesn't exist
        const data = {
          issuer_pubkey: event.data[0].toString(),
          receiver_pubkey: event.data[1].toString(),
          amount: event.data[2].toNumber(),
          created_at: created_at,
          created_on: blockIndex
        }
        await app.sql`INSERT INTO transaction ${app.sql(data)}`
        app.log.warn(
          `[${blockIndex}] Insert transaction ${data.issuer_pubkey} -> ${data.receiver_pubkey}`
        )
      } catch (error) {
        app.log.error(error)
      }
    }
  )
}
