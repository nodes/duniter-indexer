import type { FastifyInstance } from 'fastify'

export default async (app: FastifyInstance) => {
  async function addCert({
    blockIndex,
    created_at,
    args,
    event,
    signer
  }: newCertArgs) {
    const data = {
      issuer_index: Number(event.data[0]),
      receiver_index: Number(event.data[1]),
      created_at: created_at,
      created_on: blockIndex
    }
    app.log.info(
      `[${blockIndex}] Insert certification ${data.issuer_index} -> ${data.receiver_index}`
    )
    await app.sql`INSERT INTO certification ${app.sql(data)}`
  }

  app.blockEvent.on('cert.NewCert', addCert)
  app.blockEvent.on('cert.RenewedCert', addCert)
}
