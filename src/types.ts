// type eventArgs = {
//     blockIndex: number,
//     blockHash: string,
//     created_at: Date,
//     event: any,
//     args: any,
//     signer: string,
// }

type insertAccountArgs = {
  blockIndex: number
  created_at: Date
  event: any
}

type newCertArgs = {
  blockIndex: number
  created_at: Date
  args: any
  event: any
  signer: string
}

type idtyCreatedArgs = {
  blockIndex: number
  created_at: Date
  args: any
  event: any,
  signer: string
}

type idtyConfirmedArgs = {
  blockIndex: number
  created_at: Date
  args: any
  signer: string
}

type idtyValidatedArgs = {
  blockIndex: number
  created_at: Date
  signer: string
}

type idtyChangedOwnerKeyArgs = {
  blockIndex: number
  event: any
  signer: string
}

type idtyRemovedArgs = {
  blockIndex: number
  created_at: Date
  signer: string
}
