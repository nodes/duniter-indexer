import { fastify as Fastify, FastifyServerOptions } from 'fastify'
import config from './config'
import { EnvSchemaData } from 'env-schema'

export default async (opts: FastifyServerOptions = {}) => {
  const app = Fastify(opts)

  // Set rate limit to 100 requests per second
  app.register(import('@fastify/rate-limit'), {
    max: import.meta.env.PROD ? 100 : Infinity,
    timeWindow: '1 minute'
  })

  // Register postgres
  const schema = config as EnvSchemaData
  const options = {
    POSTGRES_HOST: schema.POSTGRES_HOST as string,
    POSTGRES_USER: schema.POSTGRES_USER as string,
    POSTGRES_PASSWORD: schema.POSTGRES_PASSWORD as string,
    POSTGRES_DB: schema.POSTGRES_DB as string,
    POSTGRES_PORT: schema.POSTGRES_PORT as string
  }
  app.register(import('@/plugins/sql'), options)

  // Register graphiql plugin
  app.register(import('@/plugins/graphiql'), {
    INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL:
      config.INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL,
    INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL:
      config.INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL
  })

  // Decorate fastify instance with polkadot api
  app.register(import('@/plugins/polkadot'), {
    endpoint: config.INDEXER_DUNITER_WS_ENDPOINT
  })

  // Register block events processors
  app.register(import('@/plugins/blockEventEmitter'))
  app.register(import('@/processors/account'))
  app.register(import('@/processors/identity'))
  app.register(import('@/processors/certification'))
  app.register(import('@/processors/smith'))
  app.register(import('@/processors/transaction'))

  // All plugins loaded, start indexing and watching for new blocks
  app.register(import('@/services/start-indexer'))

  return app
}
