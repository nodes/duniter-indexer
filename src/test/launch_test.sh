#!/bin/bash

testName=$1
option=$2
[[ ! $testName ]] && testName='indexer_complete'

## Start local Duniter node
pnpm clean-db
docker compose stop duniter-rpc
rm -rf duniter-data/chains
docker-compose -f docker-compose.yaml -f docker-compose.test.yaml up -d

# Start integration test
pnpm test

# Stop
# docker compose stop duniter-rpc
