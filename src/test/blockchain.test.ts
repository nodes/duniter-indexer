import app from '@/test'
import { Keyring } from '@polkadot/api'
import { web3Accounts, web3FromSource } from '@polkadot/extension-dapp'
import { stringToHex } from '@polkadot/util'
import { describe, expect, it } from 'vitest'

const badBlockHash =
  '0x0000000000000000000000000000000000000000000000000000000000000000'

describe('Test chain', async () => {
  it('should have block', async () => {
    const res = await app.polkadot.rpc.chain.getBlockHash(1)
    expect(res.toString()).not.toBe(badBlockHash)
  })

  it('should have identity ManUtopiK', async () => {
    const res = await app.polkadot.query.identity.identityIndexOf(
      '5DUjwHRqPayt3tAZk1fqEgU99xZB9jzBHKy2sMSTNcc7m9D1'
    )
    expect(res.toJSON()).toBe(1)
  })

  it('should create block', async () => {
    // await app.polkadot.rpc.engine.createBlock(true, true)

    const finalized_block_hash = (
      await app.polkadot.rpc.chain.getFinalizedHead()
    ).toString()
    const lastBlock = await app.polkadot.rpc.chain.getBlock(
      finalized_block_hash
    )
    const lastBlockNumber = lastBlock.block.header.number.toNumber()

    expect(lastBlockNumber).toBe(2)
  })

  //   final json = await sdk.api.keyring
  //   .importAccount(keyring,
  //       keyType: keytype,
  //       key: generatedMnemonic,
  //       name: derivePath,
  //       password: password,
  //       derivePath: derivePath,
  //       cryptoType: CryptoType.sr25519)
  //   .catchError((e) {
  // importIsLoading = false;
  // notifyListeners();
  // });
  // if (json == null) return '';
  // // log.d(json);
  // try {
  // await sdk.api.keyring.addAccount(
  //   keyring,
  //   keyType: keytype,
  //   acc: json,
  //   password: password,
  // );

  const mnemonic =
    'pipe paddle ketchup filter life ice feel embody glide quantum ride usage'

  const test1 = '5FeggKqw2AbnGZF9Y9WPM2QTgzENS3Hit94Ewgmzdg5a3LNa' // , 'test1');
  const test2 = '5E4i8vcNjnrDp21Sbnp32WHm2gz8YP3GGFwmdpfg5bHd8Whb' // , 'test2');
  const test3 = '5FhTLzXLNBPmtXtDBFECmD7fvKmTtTQDtvBTfVr97tachA1p' // , 'test3');
  const test4 = '5DXJ4CusmCg8S1yF6JGVn4fxgk5oFx42WctXqHZ17mykgje5' // , 'test4');
  const test5 = '5Dq3giahrBfykJogPetZJ2jjSmhw49Fa7i6qKkseUvRJ2T3R' // , 'test5');
  const test6 = '5GxEp3do81j97kNaH4JyZgDXuPoKWoTuxXXWGyyNXeKeVLHb' // , 'test6');
  const test7 = '5FZ1sSvREbQLCtSSCvMUx7KCAnpJkB7q5mfz2oixiZq2ChET' // , 'test7');
  const test8 = '5CoKV9EEgwb2NmWamTXUAa6ycfNb2k1iNfVGvJAkg7dLq9RH' // , 'test8');

  it('change owner key', async () => {
    const keyring = new Keyring({ type: 'sr25519' })
    const seed = keyring.addFromMnemonic(mnemonic)
    const keyPair = seed.derive('//2')

    const string = 'icok'
    const prefix = Uint8Array.from(string.split('').map((x) => x.charCodeAt(0)))
    const genesisHash = await app.polkadot.genesisHash.toHex()

    const idtyIndex =
      '000' +
      (await app.polkadot.query.identity.identityIndexOf(keyPair.address))
    const oldPubkey = keyPair.publicKey

    const messageToSign = prefix + genesisHash + idtyIndex + oldPubkey
    const messageToHex = Buffer.from(messageToSign).toString('hex')

    const allAccounts = await web3Accounts()
    const account = allAccounts[0]
    const injector = await web3FromSource(account.meta.source)
    const signRaw = injector?.signer?.signRaw

    if (!!signRaw) {
      // after making sure that signRaw is defined
      // we can use it to sign our message
      const { signature } = await signRaw({
        address: account.address,
        data: stringToHex(messageToSign),
        type: 'bytes'
      })
      console.log(signature)
    }

    const signedMessage = keyPair.sign(messageToHex)
    const resp = await app.polkadot.tx.identity.changeOwnerKey(
      test2,
      signedMessage
    )
    console.log(resp.toString())
    await app.polkadot.rpc.engine.createBlock(true, true)
  })
})
