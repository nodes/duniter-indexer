import { describe, it, expect } from 'vitest'
import app from '@/test'

describe('Test postgresql', async () => {
  it('should respond', async () => {
    const res = await app.sql`
      SELECT value as last_indexed_block_number
      FROM parameters
      WHERE key = 'last_indexed_block_number'
    `
    expect(res).toBeDefined()
  })
})

describe('Test polkadot', async () => {
  it('should respond', async () => {
    const res = (await app.polkadot.rpc.chain.getFinalizedHead()).toString()
    expect(res).toBeDefined()
  })
})
