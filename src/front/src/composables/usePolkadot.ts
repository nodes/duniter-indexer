import { ApiPromise, WsProvider } from '@polkadot/api'

// Starting polkadot outside composable to trigger it once
let polkadot = null
const endpoint = window.INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL || 'ws://localhost:9944'
const wsProvider = new WsProvider(endpoint)

export async function usePolkadot() {
  // Construct
  polkadot = await ApiPromise.create({ provider: wsProvider })

  // Expose polkadot on window to use it in javascript devtools console for dev purpose
  console.log(`Endpoint duniter RPC connected : ${endpoint}.\nUse \`polkadot\` variable in devtools for dev purpose.`)
  window.polkadot = polkadot

  return polkadot
}