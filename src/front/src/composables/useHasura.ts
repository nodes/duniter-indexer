import { computed, ref } from 'vue'

// Use computed outside composable to use it like a store
const endpoint = window.INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL || 'http://localhost:8080'
const relay = ref(false)
const graphqlApi = computed(() => endpoint + (relay.value ? '/v1beta1/relay' : '/v1/graphql'))

export function useHasura() {
  return { graphqlApi, relay }
}