import { createApp } from 'vue'
import App from './App.vue'
import { anu } from 'anu-vue'

// UnoCSS import
import 'uno.css'

// import styles
import 'anu-vue/dist/style.css'
import './style.css'

// Using `app.use(anu)` will register all the components globally
createApp(App)
  .use(anu)
  .mount('#app')
