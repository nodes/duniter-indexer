import envSchema from 'env-schema'

const schema = {
  type: 'object',
  properties: {
    POSTGRES_HOST: {
      type: 'string',
      default: 'localhost'
    },
    POSTGRES_USER: {
      type: 'string',
      default: 'postgres'
    },
    POSTGRES_PASSWORD: {
      type: 'string',
      default: 'postgrespassword'
    },
    POSTGRES_DB: {
      type: 'string',
      default: 'postgres'
    },
    INDEXER_DUNITER_WS_ENDPOINT: {
      type: 'string',
      default: 'ws://localhost:9944'
    },
    INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL: {
      type: 'string',
      default: 'ws://localhost:9944'
    },
    INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL: {
      type: 'string',
      default: 'http://localhost:8080'
    },
    INDEXER_GENESIS_FILE_PATH: {
      type: 'string',
      default: 'resources/specs/gdev.json'
    },
    INDEXER_TRANSACTIONS_FILE_PATH: {
      type: 'string',
      default: 'resources/history.json'
    }
  }
}

const options = {
  dotenv: true,
  schema
}

export default envSchema(options)
