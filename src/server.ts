import envSchema from 'env-schema'
import fastify from './app'
import logger from './logger'

const start = async () => {
  const server = await fastify({
    logger,
    pluginTimeout: 50000,
    bodyLimit: 15485760
  })

  // Get port from .env or 3000
  const { PORT } = envSchema<{ PORT: number }>({
    dotenv: true,
    schema: {
      type: 'object',
      required: ['PORT'],
      properties: {
        PORT: {
          type: 'number',
          default: 3000
        }
      }
    }
  })

  try {
    server.listen({ host: '0.0.0.0', port: PORT })
  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}

start()
