import fp from 'fastify-plugin'
import { ApiPromise, WsProvider } from '@polkadot/api'
import type { FastifyInstance } from 'fastify'

declare module 'fastify' {
  interface FastifyInstance {
    polkadot: ApiPromise
  }
}

async function polkadot(app: FastifyInstance, opts: any, done: () => void) {
  const provider = new WsProvider(opts.endpoint)
  const polkadot = await ApiPromise.create({
    provider
  })
  app.decorate('polkadot', polkadot)

  done()
}

export default fp(polkadot)
