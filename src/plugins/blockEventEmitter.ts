import fp from 'fastify-plugin'
import EventEmitter from 'events'
import type { FastifyInstance } from 'fastify'

declare module "fastify" {
  interface FastifyInstance {
    blockEvent: EventEmitter
  }
}

async function blockEventEmitter(app: FastifyInstance, options: unknown, done: () => void) {
  const blockEvent = new EventEmitter()

  app.decorate('blockEvent', blockEvent)

  done()
}

export default fp(blockEventEmitter)
