import type { FastifyInstance } from 'fastify'
import { dirname, resolve } from 'node:path'
import { fileURLToPath } from 'node:url'

const __dirname = dirname(fileURLToPath(import.meta.url))

export default function (
  app: FastifyInstance,
  options: {
    [key: string]: string | number | boolean | undefined | null | unknown
  },
  done: () => void
) {
  // Serve frontend
  app.register(import('@fastify/static'), {
    root: resolve(__dirname, '../front/')
  })

  app.get('/config.js', (req, reply) => {
    reply.header('Content-Type', 'application/javascript')
      .send(`window.INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL = '${options.INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL}';
             window.INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL = '${options.INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL}';`)
  })

  done()
}
