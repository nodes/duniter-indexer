import fp from 'fastify-plugin'
import postgres, { PostgresType } from 'postgres'
import type { FastifyInstance } from 'fastify'

declare module "fastify" {
  interface FastifyInstance {
    sql: postgres.Sql<Record<string, unknown>>
  }
}

interface sqlPluginOptions {
  POSTGRES_HOST: string
  POSTGRES_USER: string
  POSTGRES_PASSWORD: string
  POSTGRES_DB: string
}

async function sql(app: FastifyInstance, options: sqlPluginOptions, done: () => void) {
  const sql = postgres({
    host: options.POSTGRES_HOST,
    username: options.POSTGRES_USER,
    password: options.POSTGRES_PASSWORD,
    database: options.POSTGRES_DB,
    debug: true
  })

  app.decorate('sql', sql)

  app.addHook('onClose', async () => {
    if (!import.meta.env.TEST) await sql.end({ timeout: 5 })
  })

  done()
}

export default fp(sql)
