import fs from 'fs'

const environment = (import.meta.env.MODE || 'development') as 'development' | 'production' | 'test'

// Set logger options
const logger = {
  production: {
    level: 'warn',
    file: './logs/production-logs.log',
  },
  development: {
    transport: {
      target: 'pino-pretty',
      options: {
        translateTime: 'HH:MM:ss.l',
        ignore: 'pid,hostname'
      }
    }
  },
  test: false,
}

// Create dir for logs
const logDir = './logs'

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir, { recursive: true })
}

export default logger[environment] ?? true
