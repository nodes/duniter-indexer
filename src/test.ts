import { beforeAll, afterAll } from 'vitest'
import fastify from './app'

const app = await fastify()

beforeAll(async () => {
  // called once before all tests run
  await app.ready()
})
afterAll(async () => {
  // called once after all tests run
  await app.close()
})

export default app
