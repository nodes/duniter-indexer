## Reset the database

```
pnpm clean-db
```

## Revert to a block

If you want to resume indexing before a given block:

```sql
DELETE FROM block WHERE number>19550;
UPDATE parameters SET value='19550' WHERE key='last_indexed_block_number';
```