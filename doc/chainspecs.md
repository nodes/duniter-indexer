# Chainspecs

DuniterIndexer input data is the output of Duniter `build-spec` command.

```sh
# duniter binary with default runtime (gdev) and default chain (dev)
cargo run -- build-spec --chain dev 1> ./resources/specs/dev.json
# duniter binary with [gtest] runtime and [gtest_dev] chain
cargo run --features gtest --no-default-features -- build-spec --chain gtest_dev 1> ./resources/specs/gtest_dev.json
```

Then the indexer can be run with:

```sh
# start the duniter node (here dev)
cargo run --release -- --chain ./resources/specs/dev.json --tmp --alice --pruning archive --force-authoring
# first clean the postgres database
pnpm clean-db
# then index with given chainspecs
INDEXER_GENESIS_FILE_PATH=./resources/specs/dev.json INDEXER_TRANSACTIONS_FILE_PATH=resources/history_test.json pnpm dev
```