Extra readme sections were moved to this file and not reviewed in detail for now.


### Using the dev blockchain (longer)

**1. Start the stack**

Start docker (hasura + postgresql + duniter-v2s stack):

```bash
docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up -d
```

It will start three services :

- [a duniter-v2s rpc mirror in archive mode (http rpc on 9933, ws rpc on 9944, p2p on 30333)](https://polkadot.js.org/apps/?rpc=ws%3A%2F%2Flocalhost:9944%2Fws%2Fexplorer#/explorer)
- a postgres database (on port 5432)
- [a hasura graphql server (on port 8080)](http://localhost:8080/console) (secret is "your_secret") (readonly mode)

**2. Start the Indexer App**

- Start dev mode using Vite:

To start the indexer in dev mode, run

```bash
pnpm dev
```

To start the front indexer in dev mode, run

```bash
pnpm dev:front
```

**_WARNING: The full indexation should take ~30 min on the local node._**

- Start test mode using Vitest:

```bash
pnpm test
```

- Start test mode with coverage:

```bash
pnpm coverage
```

- Compile typescript to javascript:

```bash
pnpm build
```

### Indexing

You should see the app indexing blocks in the output logs:

```bash
[20:21:12.328] INFO: Server listening at http://0.0.0.0:3000
[20:21:12.338] INFO: Indexing past blocks...
[20:21:12.877] INFO: Has some blocks to index.
[20:21:15.062] INFO: Indexing block#100
...
```

### Troubleshooting

- ERROR: duplicate key value violates unique constraint "parameters_pkey"

You tried to index the blockchain twice. You should proceed to a complete reset and restart the stack:

```bash

docker-compose down --volumes --remove-orphans
docker-compose -f docker-compose.yaml -f docker-compose.xxx.yaml build
```

**_WARNING: Replace xxx by the profile you want to use (dev, test or prod)._**

- ERROR: relation "parameters" does not exist

You tried to start the app before the blockchain was fully synced. Wait for at least block number #5 to start the app.

### Development for Hasura

Open hasura console:

```bash
pnpm hc
```

Dev mode at <http://localhost:9695> : open new terminal and:

## Code

- Main file: `src/app.ts`
- Configure logger: `src/logger.ts`
- Change port in `.env`

### Docs

- [Vite](https://vitejs.dev/)
- [Vite Plugin Node](https://github.com/axe-me/vite-plugin-node)
- [Vitest](https://vitest.dev/)
- [Fastify](https://www.fastify.io/docs/latest/)

### Ressources

- [Awesome vite](https://github.com/vitejs/awesome-vite)
- [Fastify ecosystem](https://www.fastify.io/ecosystem/)

## Duniter v2s and Polkadot

### create block manualy

rpc.engine.createBlock(true/false si données à écrire, true)

### index des pallets

/home/manu/Sites/g1/duniter-v2s/runtime/gdev/src/lib.rs#245
