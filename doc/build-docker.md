FIXME check how to build a full docker image


### Complete reset

Shutdown all Indexer's containers and remove any data:

```bash
docker-compose down --volumes --remove-orphans
```

The `--volumes` option erases any previous data from the stack (i.e. both indexed data and blockchain data).

Then rebuild the full Indexer image:

```bash
docker-compose -f docker-duniter-gdev.yml -f docker-dep-prod.yml build
```

Restarting the stack will then engage a new full synchronization.

Alternatively, you can also use the `--no-cache` option to rebuild the stack without using any cache.