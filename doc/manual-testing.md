# Manual testing

## Ğcli

Ğcli is a command line client written in Rust with `subxt`: https://git.duniter.org/clients/rust/gcli-v2s/.
Here are some commands useful for testing indexer:

```sh
# smith certification from "Alice" to "Charlie" 
gcli -S predefined -s Alice smith cert 3
```

## Ğecko

To generate events for testing, you can use the [Gecko mobile apk](https://forum.duniter.org/t/gecko-gdev-last-build/9367/last) that you should install on your phone.
Once installed, you should first change the server, in the top left corner menu > Parameters > Network Settings: Node > Custom server to the one of your local indexer app by setting `ws://192.168.X.X:9944/ws` where **192.168.X.X** is your local IP address that you can find by using: `ip a` (linux) or `ipconfig -a` (windows).

Then, you should import the test chest by using the following [mnemonic](/src/test/blockchain.test.ts#L60):

`bottom drive obey lake curtain smoke basket hold race lonely fit walk`

## Vscode debugging

You can use vscode debugging tool to see the state of a variable at a given breakpoint:

![debugging](./img/vscode-debugging.png)