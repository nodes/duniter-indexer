alter table "public"."account"
  add constraint "account_id_fkey"
  foreign key ("id")
  references "public"."identity"
  ("id") on update restrict on delete restrict;
