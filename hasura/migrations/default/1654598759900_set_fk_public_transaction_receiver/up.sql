alter table "public"."transaction"
  add constraint "transaction_receiver_fkey"
  foreign key ("receiver")
  references "public"."identity"
  ("id") on update cascade on delete cascade;
