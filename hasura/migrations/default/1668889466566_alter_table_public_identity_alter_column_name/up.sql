alter table "public"."identity" alter column "name" set not null;
alter table "public"."identity" add constraint "identity_name_key" unique ("name");
