comment on column "public"."certification"."receiver_name" is E'Table of certifications.';
alter table "public"."certification" alter column "receiver_name" drop not null;
alter table "public"."certification" add column "receiver_name" text;
