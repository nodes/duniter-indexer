DROP FUNCTION public.is_member(account_row account)
/* RETURNS boolean
 LANGUAGE sql
 STABLE
AS $function$
  SELECT EXISTS (
    SELECT 1
    FROM public.identity i
    WHERE i.id = account_row.id
)
$function$*/;
