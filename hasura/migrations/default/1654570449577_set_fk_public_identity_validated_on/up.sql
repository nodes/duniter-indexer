alter table "public"."identity"
  add constraint "identity_validated_on_fkey"
  foreign key ("validated_on")
  references "public"."block"
  ("index") on update no action on delete no action;
