alter table "public"."identity" drop constraint "identity_id_fkey",
  add constraint "identity_id_fkey"
  foreign key ("id")
  references "public"."account"
  ("id") on update restrict on delete restrict;
