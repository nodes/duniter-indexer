SET check_function_bodies = false;
CREATE TABLE public.certification (
    issuer text NOT NULL,
    receiver text NOT NULL,
    created_at timestamp with time zone NOT NULL
);
CREATE TABLE public.identity (
    id text NOT NULL,
    name text,
    idty_index integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    confirmed_at timestamp with time zone,
    validated_at timestamp with time zone,
    revoked_at timestamp with time zone
);
CREATE TABLE public.parameters (
    key text NOT NULL,
    value jsonb
);
ALTER TABLE ONLY public.certification
    ADD CONSTRAINT certification_pkey PRIMARY KEY (issuer, receiver);
ALTER TABLE ONLY public.identity
    ADD CONSTRAINT identity_key_key UNIQUE (id);
ALTER TABLE ONLY public.identity
    ADD CONSTRAINT identity_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_pkey PRIMARY KEY (key);
ALTER TABLE ONLY public.certification
    ADD CONSTRAINT certification_issuer_fkey FOREIGN KEY (issuer) REFERENCES public.identity(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.certification
    ADD CONSTRAINT certification_receiver_fkey FOREIGN KEY (receiver) REFERENCES public.identity(id) ON UPDATE CASCADE ON DELETE CASCADE;
