alter table "public"."certification" drop constraint "certification_created_on_fkey",
  add constraint "certification_created_on_fkey"
  foreign key ("created_on")
  references "public"."block"
  ("number") on update cascade on delete cascade;
