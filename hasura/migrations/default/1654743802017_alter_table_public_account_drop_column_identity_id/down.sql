comment on column "public"."account"."identity_id" is E'Table of accounts.';
alter table "public"."account"
  add constraint "account_identity_id_fkey"
  foreign key (identity_id)
  references "public"."identity"
  (id) on update cascade on delete set null;
alter table "public"."account" alter column "identity_id" drop not null;
alter table "public"."account" add column "identity_id" text;
