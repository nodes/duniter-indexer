alter table "public"."transaction"
  add constraint "transaction_issuer_fkey"
  foreign key ("issuer")
  references "public"."identity"
  ("id") on update cascade on delete cascade;
