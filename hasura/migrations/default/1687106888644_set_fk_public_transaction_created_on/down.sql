alter table "public"."transaction" drop constraint "transaction_created_on_fkey",
  add constraint "transaction_created_on_fkey"
  foreign key ("created_on")
  references "public"."block"
  ("number") on update cascade on delete cascade;
