alter table "public"."certification"
  add constraint "certification_issuer_index_fkey"
  foreign key ("issuer_index")
  references "public"."identity"
  ("index") on update cascade on delete cascade;
