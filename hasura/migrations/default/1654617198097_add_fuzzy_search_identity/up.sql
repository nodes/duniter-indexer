CREATE EXTENSION pg_trgm;

CREATE INDEX identity_gin_idx ON identity
USING GIN ((name) gin_trgm_ops);

CREATE OR REPLACE FUNCTION public.search_identity(name text)
 RETURNS SETOF identity
 LANGUAGE sql
 STABLE
AS $function$
    SELECT *
    FROM public.identity AS i
    WHERE
      $1 <% (i.name)
    ORDER BY
      Similarity($1, (i.name)) DESC
$function$