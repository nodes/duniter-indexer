comment on table "public"."identity" is NULL;

comment on column "public"."block"."index" is NULL;

comment on column "public"."block"."data" is E 'Data contains `extrinsics` and `events` of the block. Exemple for querying specific data: `data(path: "events[0].event")`.';

comment on column "public"."block"."data" is NULL;

comment on table "public"."block" is NULL;

comment on column "public"."certification"."created_on" is NULL;

comment on table "public"."certification" is NULL;

comment on column "public"."identity"."revoked_on" is NULL;

comment on column "public"."identity"."validated_on" is NULL;

comment on column "public"."identity"."confirmed_on" is NULL;

comment on column "public"."identity"."created_on" is NULL;

comment on column "public"."transaction"."created_on" is E 'Block number where transaction was executed.';

comment on column "public"."identity"."name" is E 'Name of the identity.';

comment on column "public"."identity"."name" is NULL;

comment on column "public"."identity"."id" is NULL;

comment on table "public"."identity" is NULL;

comment on table "public"."identity" is NULL;

comment on table "public"."parameters" is NULL;

comment on table "public"."parameters" is NULL;

comment on column "public"."transaction"."amount" is NULL;

comment on column "public"."transaction"."created_on" is NULL;

comment on column "public"."transaction"."id" is NULL;

comment on table "public"."transaction" is NULL;

comment on function "public"."search_identity" is NULL;

comment on function "public"."search_identity" is NULL;