comment on function "public"."search_identity" is E'Fuzzy search identity by name';

comment on table "public"."transaction" is E'Table of transactions.';

comment on column "public"."transaction"."id" is E'Primary Key `id` is used for postgreSQL and Hasura relationship, not related to any value in substrate.';

comment on column "public"."transaction"."created_on" is E'Block number where transaction was executed.';

comment on column "public"."transaction"."amount" is E'Amount of the transaction. In milliĞ1. 1000 = 1 Ğ1.';

comment on table "public"."parameters" is E'List of parameters';

comment on table "public"."parameters" is E'List of key/value parameters.';

comment on table "public"."identity" is E'Table of identities.';

comment on table "public"."identity" is E'Table of account. Could be a simple account or an identity.';

comment on column "public"."identity"."id" is E'PubKey of the account.';

comment on column "public"."identity"."name" is E'Name of the identity.';

comment on column "public"."identity"."name" is E'Name for identity.';

comment on column "public"."transaction"."created_on" is E'Block number where transaction was created.';

comment on column "public"."identity"."created_on" is E'Block number where account was created.';

comment on column "public"."identity"."confirmed_on" is E'Block number where account was confirmed.';

comment on column "public"."identity"."validated_on" is E'Block number where account was validated.';

comment on column "public"."identity"."revoked_on" is E'Block number where account was revoked.';

comment on table "public"."certification" is E'Table of certifications.';

comment on column "public"."certification"."created_on" is E'Block number where certification was created.';

comment on table "public"."block" is E'Table of blocks.';

comment on column "public"."block"."data" is E'Data contains `extrinsics` and `events` of the block. Exemple for querying specific data: `data(path: "events[0].event")`.';

comment on column "public"."block"."data" is E'Data contains `extrinsics` and `events` of the block. Exemple for querying specific data: `data(path: "extrinsics[0].isSigned")`.';

comment on column "public"."block"."index" is E'Index of the block in substrate.';

comment on table "public"."identity" is E'Table of accounts. Can be a simple account or an identity.';

comment on function "public"."search_identity" is E'Fuzzy search identity by name.';