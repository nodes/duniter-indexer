BEGIN TRANSACTION;
ALTER TABLE "public"."certification" DROP CONSTRAINT "certification_pkey";

ALTER TABLE "public"."certification"
    ADD CONSTRAINT "certification_pkey" PRIMARY KEY ("created_on", "issuer_index", "receiver_index");
COMMIT TRANSACTION;
