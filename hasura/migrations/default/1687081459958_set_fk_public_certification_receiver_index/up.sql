alter table "public"."certification"
  add constraint "certification_receiver_index_fkey"
  foreign key ("receiver_index")
  references "public"."identity"
  ("index") on update cascade on delete cascade;
