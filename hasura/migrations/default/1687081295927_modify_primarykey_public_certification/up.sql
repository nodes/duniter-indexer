BEGIN TRANSACTION;
ALTER TABLE "public"."certification" DROP CONSTRAINT "certification_pkey";

ALTER TABLE "public"."certification"
    ADD CONSTRAINT "certification_pkey" PRIMARY KEY ("receiver_name", "issuer_name", "created_on");
COMMIT TRANSACTION;
