comment on column "public"."identity"."created_on" is E'Table of identities.';
alter table "public"."identity"
  add constraint "identity_created_on_fkey"
  foreign key (created_on)
  references "public"."block"
  (index) on update no action on delete no action;
alter table "public"."identity" alter column "created_on" drop not null;
alter table "public"."identity" add column "created_on" int4;
