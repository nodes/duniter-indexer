alter table "public"."certification" drop constraint "certification_issuer_name_fkey",
  add constraint "certification_issuer_fkey"
  foreign key ("issuer_name")
  references "public"."identity"
  ("id") on update cascade on delete cascade;
