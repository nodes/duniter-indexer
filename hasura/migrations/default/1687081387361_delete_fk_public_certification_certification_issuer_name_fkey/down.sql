alter table "public"."certification"
  add constraint "certification_issuer_name_fkey"
  foreign key ("issuer_name")
  references "public"."identity"
  ("name") on update cascade on delete cascade;
