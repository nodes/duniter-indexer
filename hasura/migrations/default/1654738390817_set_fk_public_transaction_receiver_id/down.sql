alter table "public"."transaction" drop constraint "transaction_receiver_id_fkey",
  add constraint "transaction_receiver_fkey"
  foreign key ("receiver_id")
  references "public"."identity"
  ("id") on update cascade on delete cascade;
