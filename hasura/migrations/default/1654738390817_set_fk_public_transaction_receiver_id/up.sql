alter table "public"."transaction" drop constraint "transaction_receiver_fkey",
  add constraint "transaction_receiver_id_fkey"
  foreign key ("receiver_id")
  references "public"."account"
  ("id") on update cascade on delete restrict;
