BEGIN TRANSACTION;
ALTER TABLE "public"."account" DROP CONSTRAINT "account_pkey";

ALTER TABLE "public"."account"
    ADD CONSTRAINT "account_pkey" PRIMARY KEY ("id");
COMMIT TRANSACTION;
