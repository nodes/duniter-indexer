alter table "public"."account" drop constraint "account_pkey";
alter table "public"."account"
    add constraint "account_pkey"
    primary key ("identity_id");
