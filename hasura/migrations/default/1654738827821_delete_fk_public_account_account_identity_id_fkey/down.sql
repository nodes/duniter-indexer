alter table "public"."account"
  add constraint "account_identity_id_fkey"
  foreign key ("identity_id")
  references "public"."identity"
  ("id") on update cascade on delete set null;
