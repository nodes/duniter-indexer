alter table "public"."account" drop constraint "account_created_on_fkey",
  add constraint "account_created_on_fkey"
  foreign key ("created_on")
  references "public"."block"
  ("number") on update cascade on delete cascade;
