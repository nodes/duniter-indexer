CREATE TABLE "public"."block" ("index" integer NOT NULL, "hash" text NOT NULL, "created_at" Timestamp NOT NULL, "data" jsonb, PRIMARY KEY ("index") , UNIQUE ("index"), UNIQUE ("hash"));
