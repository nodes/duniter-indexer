comment on column "public"."identity"."created_at" is E'Table of identities.';
alter table "public"."identity" alter column "created_at" drop not null;
alter table "public"."identity" add column "created_at" timestamptz;
