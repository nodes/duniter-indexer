alter table "public"."certification" drop constraint "certification_pkey";
alter table "public"."certification"
    add constraint "certification_pkey"
    primary key ("created_on", "receiver_name", "issuer_name");
