alter table "public"."transaction" drop constraint "transaction_issuer_fkey",
  add constraint "transaction_issuer_id_fkey"
  foreign key ("issuer_id")
  references "public"."account"
  ("id") on update cascade on delete restrict;
