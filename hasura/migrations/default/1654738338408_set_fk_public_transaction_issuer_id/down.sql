alter table "public"."transaction" drop constraint "transaction_issuer_id_fkey",
  add constraint "transaction_issuer_fkey"
  foreign key ("issuer_id")
  references "public"."identity"
  ("id") on update cascade on delete cascade;
