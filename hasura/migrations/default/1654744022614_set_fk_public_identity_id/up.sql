alter table "public"."identity"
  add constraint "identity_id_fkey"
  foreign key ("id")
  references "public"."account"
  ("id") on update restrict on delete restrict;
