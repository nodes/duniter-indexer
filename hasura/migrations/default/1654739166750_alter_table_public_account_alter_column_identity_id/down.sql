alter table "public"."account" drop constraint "account_identity_id_key";
alter table "public"."account" alter column "identity_id" set not null;
