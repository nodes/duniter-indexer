alter table "public"."account" alter column "identity_id" drop not null;
alter table "public"."account" add constraint "account_identity_id_key" unique ("identity_id");
