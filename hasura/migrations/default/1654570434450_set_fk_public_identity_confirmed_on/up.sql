alter table "public"."identity"
  add constraint "identity_confirmed_on_fkey"
  foreign key ("confirmed_on")
  references "public"."block"
  ("index") on update no action on delete no action;
