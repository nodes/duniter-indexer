comment on column "public"."certification"."issuer_name" is E'Table of certifications.';
alter table "public"."certification" alter column "issuer_name" drop not null;
alter table "public"."certification" add column "issuer_name" text;
