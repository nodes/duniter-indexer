alter table "public"."identity"
  add constraint "identity_created_on_fkey"
  foreign key ("created_on")
  references "public"."block"
  ("number") on update cascade on delete cascade;
