alter table "public"."certification"
  add constraint "certification_receiver_name_fkey"
  foreign key ("receiver_name")
  references "public"."identity"
  ("name") on update cascade on delete cascade;
