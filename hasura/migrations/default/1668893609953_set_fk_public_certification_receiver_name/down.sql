alter table "public"."certification" drop constraint "certification_receiver_name_fkey",
  add constraint "certification_receiver_fkey"
  foreign key ("receiver_name")
  references "public"."identity"
  ("id") on update cascade on delete cascade;
