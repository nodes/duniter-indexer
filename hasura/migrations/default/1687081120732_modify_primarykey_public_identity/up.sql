BEGIN TRANSACTION;
ALTER TABLE "public"."identity" DROP CONSTRAINT "identity_pkey";

ALTER TABLE "public"."identity"
    ADD CONSTRAINT "identity_pkey" PRIMARY KEY ("index");
COMMIT TRANSACTION;
