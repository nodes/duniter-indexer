alter table "public"."identity"
  add constraint "identity_created_on_fkey"
  foreign key ("created_on")
  references "public"."block"
  ("index") on update no action on delete no action;
