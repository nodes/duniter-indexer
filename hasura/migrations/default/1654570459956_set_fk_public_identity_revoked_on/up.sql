alter table "public"."identity"
  add constraint "identity_revoked_on_fkey"
  foreign key ("revoked_on")
  references "public"."block"
  ("index") on update no action on delete no action;
